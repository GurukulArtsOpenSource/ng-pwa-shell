# Change Log

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

<a name="4.0.2"></a>
## [4.0.2](https://gitlab.com/GurukulArtsSeva/ng-pwa-shell/compare/v4.0.1...v4.0.2) (2018-07-16)



<a name="4.0.1"></a>
## [4.0.1](https://gitlab.com/GurukulArtsSeva/ng-pwa-shell/compare/v4.0.0...v4.0.1) (2018-06-03)



<a name="4.0.0"></a>
# [4.0.0](https://gitlab.com/GurukulArtsSeva/ng-pwa-shell/compare/v3.0.4...v4.0.0) (2018-06-03)



<a name="3.0.4"></a>
## [3.0.4](https://gitlab.com/GurukulArtsSeva/ng-pwa-shell/compare/v3.0.3...v3.0.4) (2018-06-02)



<a name="3.0.3"></a>
## [3.0.3](https://gitlab.com/GurukulArtsSeva/ng-pwa-shell/compare/v3.0.2...v3.0.3) (2018-06-02)



<a name="3.0.2"></a>
## [3.0.2](https://gitlab.com/GurukulArtsSeva/ng-pwa-shell/compare/v3.0.1...v3.0.2) (2018-06-02)



<a name="3.0.1"></a>
## [3.0.1](https://gitlab.com/GurukulArtsSeva/ng-pwa-shell/compare/v3.0.0...v3.0.1) (2018-06-02)



<a name="3.0.0"></a>
# [3.0.0](https://gitlab.com/GurukulArtsSeva/ng-pwa-shell/compare/v2.0.2...v3.0.0) (2018-06-02)



<a name="2.0.2"></a>
## [2.0.2](https://gitlab.com/GurukulArtsSeva/ng-pwa-shell/compare/v2.0.1...v2.0.2) (2018-06-02)



<a name="2.0.1"></a>
## [2.0.1](https://gitlab.com/GurukulArtsSeva/ng-pwa-shell/compare/v2.0.0...v2.0.1) (2018-06-02)



<a name="2.0.0"></a>
# [2.0.0](https://gitlab.com/GurukulArtsSeva/ng-pwa-shell/compare/v1.0.0...v2.0.0) (2018-06-02)



<a name="1.0.0"></a>
# [1.0.0](https://gitlab.com/GurukulArtsSeva/ng-pwa-shell/compare/v0.0.5...v1.0.0) (2018-06-02)



<a name="0.0.5"></a>
## [0.0.5](https://gitlab.com/GurukulArtsSeva/ng-pwa-shell/compare/v0.0.4...v0.0.5) (2018-06-02)



<a name="0.0.4"></a>
## [0.0.4](https://gitlab.com/GurukulArtsSeva/ng-pwa-shell/compare/v0.0.3...v0.0.4) (2018-06-02)



<a name="0.0.3"></a>
## [0.0.3](https://gitlab.com/GurukulArtsSeva/ng-pwa-shell/compare/v0.0.2...v0.0.3) (2018-06-02)



<a name="0.0.2"></a>
## [0.0.2](https://gitlab.com/GurukulArtsSeva/ng-pwa-shell/compare/v0.0.1...v0.0.2) (2018-06-02)



<a name="0.0.1"></a>
## 0.0.1 (2018-06-02)
