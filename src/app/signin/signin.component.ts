import { Component, OnInit } from '@angular/core';
import { Utilities } from '../_core/utilities';
import { AuthProviders, LoggedInUser } from '../_core/auth';
import { PushNotification, PushFilter } from '../_core/push';

import {
  AuthService,
  FacebookLoginProvider,
  GoogleLoginProvider,
  LinkedInLoginProvider
} from 'angular-6-social-login-temp';
import { environment } from '../../environments/environment';

@Component({
  selector: 'app-signin',
  templateUrl: './signin.component.html',
  styleUrls: ['./signin.component.css']
})


export class SigninComponent implements OnInit {

  ngOnInit(): void {
    //throw new Error("Method not implemented.");
    if (this.$.Auth.isLoggedIn()) this.$.Router.navigate([this.$.Auth.redirectUrl]);

  }

  constructor(private socialAuthService: AuthService, public $: Utilities) { }


  public doCustomLogin(user: string, pass: string) {
    this.$.Auth.login(AuthProviders.Custom, {u:user,p:pass}).subscribe(user => { this.afterLogin(user); }, err => { this.failedLogin(err); });
  }
  public doGoogleLogin() {
    this.$.Auth.login(AuthProviders.Google).subscribe(user => { this.afterLogin(user); }, err => { this.failedLogin(err); });
  }
  public doFacebookLogin() {
    this.$.Auth.login(AuthProviders.Facebook).subscribe(user => { this.afterLogin(user); }, err => { this.failedLogin(err); });
  }
  public doLinkedInLogin() {
    this.$.Auth.login(AuthProviders.Linkedin).subscribe(user => { this.afterLogin(user); }, err => { this.failedLogin(err); });
  }

  private afterLogin(user: any) {
    this.$.Auth.Access.push('ADMIN');
    this.$.Router.navigate([this.$.Auth.redirectUrl]);
  }

  private failedLogin(err: any) {
    console.log(err);
  }

  public doPush2() {
    let notf: PushNotification;
    notf = new PushNotification();
    notf.headings.en = "1st Push from ng";
    notf.contents.en = "Hurray!! this is a Great App now.";

    console.log("About to push");
    console.log(notf);

    this.$.Push.Push(notf).subscribe(res => console.log(res));
  }

  public doPush() {
    let notf: PushNotification;
    notf = new PushNotification();
    notf.headings.en = "1st Push from ng";
    notf.contents.en = "Hurray!! this is a Great App now.";

    let pf: PushFilter;
    pf = new PushFilter();
    pf.field = "tag";
    pf.key = "a";
    pf.relation = "=";
    pf.value = environment.SocialLoginIds.google;

    notf.filters = [pf];


    console.log("About to push");
    console.log(notf);

    this.$.Push.Push(notf).subscribe(res => console.log(res));
  }

  public doUserId() {
    console.log(this.$.Push.getUserId());
  }

  public dotstags() {
    this.$.Push.getTags().subscribe(res => console.log(res));
  }

  public dodeltags() {
    this.$.Push.deleteTags(["a", "b", "c"]).subscribe(res => console.log(res));
  }

  public dosndtags() {
    this.$.Push.sendTags({ "a": environment.SocialLoginIds.google, "e": "" }).subscribe(res => console.log(res));
  }

  public doFilter() {
    let filter;
    filter = "tag.a=77917362825-569k6obtuu5u4iidpg20pu9hveubk4np.apps.googleusercontent.com";

    this.$.Push.QuickPush("tag.fees=kswami@gurukul.org||tag.user=sswami@gurukul.org","Hello","This is Good").subscribe(res => console.log(res));

  }

}

