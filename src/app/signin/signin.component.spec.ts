import { TestBed, inject } from '@angular/core/testing';

import { SigninComponent } from './signin.component';

describe('a signin component', () => {
	let component: SigninComponent;

	// register all needed dependencies
	beforeEach(() => {
		TestBed.configureTestingModule({
			providers: [
				SigninComponent
			]
		});
	});

	// instantiation through framework injection
	beforeEach(inject([SigninComponent], (SigninComponent) => {
		component = SigninComponent;
	}));

	it('should have an instance', () => {
		expect(component).toBeDefined();
	});
});