import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';


import { ServiceWorkerModule } from '@angular/service-worker';
import { MomentModule } from 'ngx-moment';
import { HttpClientModule } from '@angular/common/http';


import { AppComponent } from './app.component';

import { environment } from '../environments/environment';

import { RouterModule, Routes } from '@angular/router'; 
import { HomeComponent } from './home/home.component';
import { SubComponent } from './sub/sub.component';
import { BaseComponent, Utilities } from './_core/utilities';

import { SigninComponent } from './signin/signin.component';




import {
  SocialLoginModule,
  AuthServiceConfig,
  GoogleLoginProvider,
  FacebookLoginProvider,
} from "angular-6-social-login-temp";
import { Service } from './_core/api';
import { CommonDataService } from './_common/common.data';
import { CommonUtilitiesService } from './_common/common.utils';
import { Md5 } from 'ts-md5';
import { LocalStorageService, LocalStorageModule } from 'angular-2-local-storage';
import { SessionStorageService } from './_core/session.storage';
import { AuthService } from './_core/auth';
import { PushService } from './_core/push';
import { CustomAuthService } from './_common/custom.auth';
import { MatSnackBarModule } from '@angular/material';
import { HotkeyModule } from 'angular2-hotkeys';
import { AuthGuard } from './_common/guard';



// Material Components
import { FlexLayoutModule } from "@angular/flex-layout";
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {
  MatButtonModule,
  MatCheckboxModule,
  MatIconModule,
  MatGridListModule,
  MatSidenavModule,
  MatToolbarModule,  
  MatCardModule,
  MatRadioModule,
  MatInputModule,
  MatListModule,
  MatMenuModule,
  MatDatepickerModule,
  MatSelectModule,
  MatSlideToggleModule,
  MatTooltipModule,  
  MatChipsModule,
  MatNativeDateModule,
  MatDialogModule,
  MatPaginatorModule,
  MatProgressBarModule,
  MatExpansionModule
} from '@angular/material';
import { TopBarComponent } from './top-bar/top-bar.component';
import { SideBarComponent } from './side-bar/side-bar.component';
import { CommonUIService } from './_common/common.ui';
import { LayoutComponent } from './layout/layout.component';
import { LabComponent } from './lab/lab.component';

const appRoutes: Routes = [
  {path: 'signin', component: SigninComponent},
  {path: '', component: LayoutComponent,
    children: [
      { path: '', redirectTo: '/home', pathMatch: 'full'  },
      { path: 'home', component: HomeComponent,canActivate: [AuthGuard] },
      { path: 'sub', component: SubComponent,canActivate: [AuthGuard], data : {has : ["ADMIN"] } },
      { path: 'lab', component: LabComponent,canActivate: [AuthGuard] },
  ]}];

// Configs 
export function getAuthServiceConfigs() {
  let config = new AuthServiceConfig(
      [
        {
          id: GoogleLoginProvider.PROVIDER_ID,
          provider: new GoogleLoginProvider(environment.SocialLoginIds.google)
        } //use other provider as per https://github.com/abacritt/angularx-social-login
      ]
  );
  return config;
}

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    SubComponent,
    SigninComponent,
    BaseComponent,
    TopBarComponent,
    SideBarComponent,
    LayoutComponent,
    LabComponent
  ],
  imports: [
    RouterModule.forRoot(
      appRoutes,
      { enableTracing: true } // <-- debugging purposes only
    ),
    BrowserModule,
    MomentModule,
    HttpClientModule,
    LocalStorageModule.withConfig({ prefix: '', storageType: 'localStorage' }),
    //ServiceWorkerModule.register('/ngsw-worker.js', { enabled: environment.production }),
    //^ Disabled as the /ngsw-worker.js will be merged & loaded with OneSignalSDKWorker.js
    SocialLoginModule,
    MatSnackBarModule,
    HotkeyModule.forRoot(),
    FlexLayoutModule,
    BrowserAnimationsModule,
    MatIconModule,
    MatGridListModule,
    MatSidenavModule,
    MatToolbarModule,
    MatButtonModule,
    MatCheckboxModule,
    MatCardModule,
    MatRadioModule,
    MatInputModule,
    MatListModule,
    MatMenuModule,
    MatDatepickerModule,
    MatSelectModule,
    MatSlideToggleModule,
    MatTooltipModule,
    MatSnackBarModule,//MaterialModule,
    MatChipsModule,
    MatNativeDateModule,
    MatDialogModule,
    MatPaginatorModule,
    MatProgressBarModule,
    MatExpansionModule
  ],
  providers: [
    {
      provide: AuthServiceConfig,
      useFactory: getAuthServiceConfigs
    },
    Utilities,
    Service,
    CommonDataService,
    CommonUtilitiesService,
    Md5,
    LocalStorageService,
    SessionStorageService,
    AuthService,
    PushService,
    CustomAuthService,
    CommonUIService,
    AuthGuard
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
