import { Component, OnInit, TemplateRef, ViewChild } from '@angular/core';
import { BaseComponent, Utilities } from '../_core/utilities';

@Component({
	selector: 'side-bar',
	templateUrl: 'side-bar.component.html',
	styles:[`.nav-active {
		background:#efefef;
		color:#2196f3;		
	}`]
})

export class SideBarComponent implements OnInit {

	@ViewChild('defaultExtras')
	private defaultExtras : TemplateRef<any>
	constructor(public $ : Utilities){

	}
	ngOnInit() { 
		this.$.UI.SidebarExtras = this.defaultExtras;
	}

	signOut() {
		this.$.Auth.logout().subscribe(res => {
			   this.$.Router.navigate(['/signin']);
		});
	}
}