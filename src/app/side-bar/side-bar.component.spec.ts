import { TestBed, inject } from '@angular/core/testing';

import { SideBarComponent } from './side-bar.component';

describe('a side-bar component', () => {
	let component: SideBarComponent;

	// register all needed dependencies
	beforeEach(() => {
		TestBed.configureTestingModule({
			providers: [
				SideBarComponent
			]
		});
	});

	// instantiation through framework injection
	beforeEach(inject([SideBarComponent], (SideBarComponent) => {
		component = SideBarComponent;
	}));

	it('should have an instance', () => {
		expect(component).toBeDefined();
	});
});