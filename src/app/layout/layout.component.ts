import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { Utilities } from "../_core/utilities";
import { MediaMatcher } from '@angular/cdk/layout';

@Component({
	selector: 'layout',
	templateUrl: 'layout.component.html'
})

export class LayoutComponent implements OnInit {

	mobileQuery: MediaQueryList;
	private _mobileQueryListener: () => void;

	constructor(private $: Utilities, changeDetectorRef: ChangeDetectorRef, media: MediaMatcher) {
		this.mobileQuery = media.matchMedia('(max-width: 600px)');
		this._mobileQueryListener = () => changeDetectorRef.detectChanges();
		this.mobileQuery.addListener(this._mobileQueryListener);
	}

	ngOnInit() { }

	ngOnDestroy(): void {
		this.mobileQuery.removeListener(this._mobileQueryListener);
	  }
}