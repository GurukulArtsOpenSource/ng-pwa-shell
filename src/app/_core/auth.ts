
import { throwError as observableThrowError, timer as observableTimer, from as observableFrom, of as observableOf, Observable, BehaviorSubject, Subscriber } from 'rxjs';

import { map, share, catchError, takeUntil, first, finalize } from 'rxjs/operators';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';

import { environment } from '../../environments/environment';
import { AuthModes, Service } from './api';

import { LocalStorageService } from 'angular-2-local-storage';
import { Md5 } from 'ts-md5/dist/md5';
import { CustomAuthService } from '../_common/custom.auth';
import { CommonUtilitiesService } from '../_common/common.utils';
import { AuthService as SocialAuthService, SocialUser, FacebookLoginProvider, GoogleLoginProvider, LinkedInLoginProvider } from "angular-6-social-login-temp";
import { now } from 'moment';


export enum AuthProviders { Custom, Google, Facebook, Linkedin }

@Injectable()
export class AuthService {


    private _isLoggedIn = false;
    private _offlineAccess = false;
    private _screenLocked = false;
    private _iAlive: number = 0;
    private _AuthProvider: AuthProviders;
    public User: LoggedInUser;
    public Access: string[] = [];
    public redirectUrl: string = "/";     // store the URL so we can redirect after logging in

    public Loggedin$: Observable<LoggedInUser>;
    private loginSubscriber: Subscriber<LoggedInUser> = new Subscriber<LoggedInUser>();

    public Loggedout$: Observable<boolean>;
    private LoggedoutSubscriber: Subscriber<boolean> = new Subscriber<boolean>();

    public Alive$: Observable<number>;
    private AliveSubscriber: Subscriber<number> = new Subscriber<number>();


    constructor(private _Api: Service, private _Router: Router, private _Cache: LocalStorageService, private _socialAuthService: SocialAuthService, public _customAuthService: CustomAuthService, private _Utils: CommonUtilitiesService) {

        this.Loggedin$ = new Observable<LoggedInUser>((observer: Subscriber<LoggedInUser>) => this.loginSubscriber = observer).pipe(share());
        this.Loggedout$ = new Observable<boolean>((observer: Subscriber<boolean>) => this.LoggedoutSubscriber = observer).pipe(share());
        this.Alive$ = new Observable<number>((observer: Subscriber<number>) => this.AliveSubscriber = observer).pipe(share());

    }

    public ngOnInit(): void {

        //  Called at AppComponent.ngOnInit() :: (Not a Native ngOnInit function)
        //  As ngOnInit doesn't execute in Services/Injectable Class
        this._customAuthService.ngOnInit();


    }

    private SetAuthProvider(Provider: AuthProviders): void {
        this._AuthProvider = Provider
        this._Cache.set('Auth.Provider', Provider);
    }

    private GetAuthProvider(): AuthProviders {
        if (this._AuthProvider != undefined) return this._AuthProvider;
        let AuthProvider: AuthProviders;
        AuthProvider = this._Cache.get('Auth.Provider');
        this._AuthProvider = AuthProvider;
        return this._AuthProvider;
    }

    private RemoveAuthProvider(): void {
        this._AuthProvider = null
        this._Cache.remove('Auth.Provider');
    }

    login(AuthProvider: AuthProviders, UserCred: any = {}): Observable<LoggedInUser> {

        this.SetAuthProvider(AuthProvider);
        let socialPlatformProvider;

        switch (AuthProvider) {
            case AuthProviders.Google: {
                socialPlatformProvider = GoogleLoginProvider.PROVIDER_ID;
                break;
            }
            case AuthProviders.Facebook: {
                socialPlatformProvider = FacebookLoginProvider.PROVIDER_ID;
                break;
            }
            case AuthProviders.Linkedin: {
                socialPlatformProvider = LinkedInLoginProvider.PROVIDER_ID;
                break;
            }
            case AuthProviders.Custom: {
                return this._customAuthService.login(UserCred).pipe(
                    map(user => {
                        this.loginSubscriber.next(user);
                        this.setUserLoginState(user);
                        return user;
                    })
                    , catchError(error => {
                        this.clearUserLoginState();
                        return observableThrowError(error);
                    })
                );
            }
            default: {
                this.clearUserLoginState();
                return observableThrowError(false);
            }
        }

        //If Not Custom

        return observableFrom(this._socialAuthService.signIn(socialPlatformProvider)).pipe(
            map(user => {
                this.loginSubscriber.next(user);
                this.setUserLoginState(user);
                return user;
            }),
            catchError(
                error => {
                    this.clearUserLoginState();
                    return observableThrowError(error);
                })
        );
    }

    isAlive(UserState: any = {}): Observable<LoggedInUser> {
        this.GetAuthProvider();

        if (!this._Utils.isOnline) {
            return this.getUserLoginState().pipe(map(user => {
                this._iAlive++;
                this.AliveSubscriber.next(this._iAlive);
                if (this._iAlive == 1 && !this._isLoggedIn) this.loginSubscriber.next(user);
                this.setUserLoginState(user);
                return user;
            })
                , catchError(error => {
                    this.clearUserLoginState();
                    return observableThrowError(error);
                })
            );
        }

        switch (this._AuthProvider) {
            case AuthProviders.Google || AuthProviders.Facebook || AuthProviders.Linkedin: {

                let gotUser;

                return this._socialAuthService.authState.pipe(
                    takeUntil(observableTimer(10000)),
                    first((v, i, s) => {
                        gotUser = v;
                        return v != null;
                    }),
                    map((user) => {
                        this._iAlive++;
                        this.AliveSubscriber.next(this._iAlive);
                        if (this._iAlive == 1 && !this._isLoggedIn) this.loginSubscriber.next(user);
                        this.setUserLoginState(user);
                        return user;
                    }),
                    catchError(error => {
                        console.log(error);
                        this.clearUserLoginState();
                        return observableThrowError(error);
                    }),
                    finalize(() => {
                        if (gotUser == null) observableThrowError("User State Expired");
                    })
                );
            }
            case AuthProviders.Custom: {
                return this._customAuthService.isAlive(UserState).pipe(
                    map(user => {
                        this._iAlive++;
                        this.AliveSubscriber.next(this._iAlive);
                        if (this._iAlive == 1 && !this._isLoggedIn) this.loginSubscriber.next(user);
                        this.setUserLoginState(user);
                        return user;
                    }),
                    catchError(error => {
                        this.clearUserLoginState();
                        return observableThrowError(error);
                    })
                );
            }
            default: {
                this.clearUserLoginState();
                return observableThrowError(false);
            }
        }



    }

    logout(UserState: any = {}): Observable<boolean> {
        this.GetAuthProvider();
        switch (this._AuthProvider) {
            case AuthProviders.Google || AuthProviders.Facebook || AuthProviders.Linkedin: {
                return observableFrom(this._socialAuthService.signOut()).pipe(
                    map(res => {
                        this.LoggedoutSubscriber.next(true);
                        this.clearUserLoginState();
                        return true;
                    }),
                    catchError(err => {
                        this.LoggedoutSubscriber.next(true);
                        this.clearUserLoginState();
                        return observableOf(true);
                    })
                );

            }
            case AuthProviders.Custom: {
                return this._customAuthService.logout(UserState).pipe(
                    map(res => {
                        this.LoggedoutSubscriber.next(true);
                        this.clearUserLoginState();
                        return true;
                    }),
                    catchError(err => {
                        this.LoggedoutSubscriber.next(true);
                        this.clearUserLoginState();
                        return observableOf(true);
                    })
                );

            }
            default: {
                this.LoggedoutSubscriber.next(true);
                this.clearUserLoginState();
                return observableOf(true);
            }
        }
    }

    public hasAccess(AccessPermission : string | string[]) : boolean {
        let permissions = [];
        if(typeof AccessPermission === "string") permissions = this.Access.filter(a => {return AccessPermission == a } );
        else permissions = this.Access.filter(a => {return AccessPermission.indexOf(a) > -1} );
        return permissions.length > 0 || AccessPermission.length == 0;
    }

    private clearUserLoginState() {
        this._Cache.remove('Auth.User');
        this._isLoggedIn = false;
        this.User = null;
        this.Access = [];
        this.LoggedoutSubscriber.next(false);
        this.RemoveAuthProvider();
    }

    private setUserLoginState(user: LoggedInUser) {
        this._Cache.set('Auth.User', user);
        this._isLoggedIn = true;
        this.User = user;
    }

    private getUserLoginState(): Observable<LoggedInUser> {
        let user: LoggedInUser;
        user = this._Cache.get('Auth.User');
        if (user) return observableOf(user);
        else return observableThrowError(false);
    }


    isLoggedIn(): boolean {
        return this._isLoggedIn;
    }


    enableOfflineAccess(pin?: string) {
        if (pin) {
            let hash = Md5.hashStr(pin);
            this._Cache.set('offlinePin', hash);
            this._offlineAccess = true;
        }
        else {
            if (this._Cache.get("offlinePin"))
                this._offlineAccess = true;
        }
    }
    disableOfflineAccess() {
        this._Cache.remove("offlinePin");
        this._offlineAccess = false;
    }
    OfflineAccess(): boolean {
        return this._offlineAccess;
    }

    validatePin(pin: string): boolean {
        let hash = Md5.hashStr(pin);
        if (hash == this._Cache.get("offlinePin")) {

            return true;
        } else { return false; }

    }
}

export declare class LoggedInUser {
    provider: string;
    id: string;
    email?: string;
    name: string;
    photoUrl?: string;
    token?: string;
    idToken?: string;
}

export interface ICustomLoginService {
    login(UserCred: any): Observable<LoggedInUser>;
    isAlive(UserState: any): Observable<LoggedInUser>;
    logout(UserState: any): Observable<boolean>;
}