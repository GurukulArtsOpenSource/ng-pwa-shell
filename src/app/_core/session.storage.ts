import { Component, Injectable } from "@angular/core";
import { LocalStorageService } from "angular-2-local-storage";
import { environment } from "../../environments/environment";

@Injectable()
export class SessionStorageService extends LocalStorageService {
    constructor () {
        super({ prefix: environment.app.prefix , storageType: 'sessionStorage' });
    }
}