
import {throwError as observableThrowError,  Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { environment } from '../../environments/environment';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { LocalStorageService } from 'angular-2-local-storage';
import { MomentModule } from 'ngx-moment';
import { now } from 'moment';
import { catchError, map } from 'rxjs/operators';





@Injectable()
export class PushService {
    private _Base_URI: string = environment.OneSignal.api;
    private _Headers: HttpHeaders;
    private _RequestOptions: { headers: HttpHeaders };
    private OneSignal: any;
    public Tags: any;


    constructor(private _http: HttpClient, private _cache: LocalStorageService) {

    }

    public ngOnInit(): void {

        //  Called at AppComponent.ngOnInit() :: (Not a Native ngOnInit function)
        //  As ngOnInit doesn't execute in Services/Injectable Class

        this.OneSignal = window['OneSignal'] || [];
        var jsOneSignal = window['OneSignal'] || [];
        var UpdateDevice = function (isPermitted) {
            if (isPermitted) {
                jsOneSignal.push(function () {
                    /* These examples are all valid */
                    jsOneSignal.getUserId().then(function (userId) {

                        if (userId) {
                            localStorage.setItem('OneSignal.DeviceId', userId);
                            console.log("OneSignal User ID:", userId);
                        }
                        else {
                            localStorage.removeItem('OneSignal.DeviceId');
                            console.log("Removed: ", userId);
                        }

                    });
                });
            }
            else {
                localStorage.removeItem('OneSignal.DeviceId');
                console.log("Removed");
            }
        }

        console.log("Init OneSignal");


        this.OneSignal.push(["init", {
            appId: environment.OneSignal.appId,
            autoRegister: true,
            //allowLocalhostAsSecureOrigin: true,
            httpPermissionRequest: {
                enable: true
            },
            notifyButton: {
                enable: false
            },
            welcomeNotification: {
                "title": "Welcome",
                "message": "Thanks for subscribing!",
                // "url": "" /* Leave commented for the notification to not open a window on Chrome and Firefox (on Safari, it opens to your webpage) */
            },
            promptOptions: {
                /* These prompt options values configure both the HTTP prompt and the HTTP popup. */
                /* actionMessage limited to 90 characters */
                actionMessage: "We'd like to show you notifications for the latest updates.",
                /* acceptButtonText limited to 15 characters */
                acceptButtonText: "Yes",
                /* cancelButtonText limited to 15 characters */
                cancelButtonText: "No"
            }
        }]);


        this.OneSignal.push(function () {
            /* These examples are all valid */
            jsOneSignal.isPushNotificationsEnabled(function (isEnabled) {
                UpdateDevice(isEnabled);
            });
        });

        this.OneSignal.push(function () {
            /* These examples are all valid */
            Notification.requestPermission(function (permission) {
                // If the user accepts, let's create a notification
                UpdateDevice(permission === "granted");
            });
        });

        this.OneSignal.push(["getNotificationPermission", function (permission) {
            UpdateDevice(permission == 'granted');
        }]);

        this.OneSignal.push(function () {
            jsOneSignal.on('notificationPermissionChange', function (permissionChange) {
                var currentPermission = permissionChange.to;
                UpdateDevice(currentPermission == 'granted');
            });
        });

        this.OneSignal.push(function () {
            jsOneSignal.on('customPromptClick', function (permissionChange) {
                var promptClickResult = permissionChange.result;
                UpdateDevice(promptClickResult == 'granted');
            });
        });

        this.OneSignal.push(function () {
            // Occurs when the user's subscription changes to a new value.
            jsOneSignal.on('subscriptionChange', function (isSubscribed) {
                UpdateDevice(isSubscribed);
            });
        });


        this.setHeaders();



    }

    public getUserId() {
        //console.log(localStorage.getItem('OneSignal.DeviceId'));
        let DeviceId: string = localStorage.getItem('OneSignal.DeviceId');
        return DeviceId;
    }

    public removeUserId() {
        localStorage.removeItem('OneSignal.DeviceId');
    }

    private setHeaders() {
        if (this._Headers == undefined) {
            this._Headers = new HttpHeaders({
                "Content-Type": "application/json",
                "Authorization": "Basic " + environment.OneSignal.restKey
            });
        }
        if (this._RequestOptions == undefined) { this._RequestOptions = { headers: this._Headers }; }

    }


    public getTags(): Observable<any> {
        let userid: string;
        if (this.getUserId()) userid = this.getUserId();
        else observableThrowError(false);
        return this._http.get(this._Base_URI + 'players/' + userid, this._RequestOptions).pipe(
            map(response => <any>response['tags']),
            catchError(error => observableThrowError(error))
        );
    }
    public sendTags(tags: any): Observable<any> {
        let userid: string;
        if (this.getUserId()) userid = this.getUserId();
        else observableThrowError(false);
        return this._http.put(this._Base_URI + 'players/' + userid, { "tags": tags }, this._RequestOptions).pipe(
            map(response => <any>response['tags']),
            catchError(error => observableThrowError(error))
        );
    }

    public deleteTags(tagkeys: string[]): Observable<any> {
        let tags = {};
        tagkeys.forEach(element => {
            tags = Object.assign(tags, JSON.parse('{ "' + element + '" : "" }'));
        });


        let userid: string;
        if (this.getUserId()) userid = this.getUserId();
        else observableThrowError(false);
        return this._http.put(this._Base_URI + 'players/' + userid, { "tags": tags }, this._RequestOptions).pipe(
            map(response => <any>response['tags']),
            catchError(error => observableThrowError(error))
        );
    }

    public Push(msg: PushNotification): Observable<any> {
        return this._http.post(this._Base_URI + 'notifications', msg, this._RequestOptions).pipe(
            map(response => <any>response),
            catchError(error => observableThrowError(error))
        );
    }

    public QuickPush(to: string, title: string, message: string, url?: string, img?: string, poster?:string): Observable<any> {
        let msg = new PushNotification();
        if(title == "" || message == "" ) return observableThrowError(false);

        msg.headings.en = title;
        msg.contents.en = message;

        if(url != "") msg.url = url;
        if(img != "") msg.chrome_web_icon = img;
        if(poster != "") msg.chrome_web_image = poster;

        if(to != "") msg.filters = msg.parseFilter(to);

        return this.Push(msg);

    }

}

export class PushNotification {
    app_id: string = environment.OneSignal.appId
    included_segments: string[] = ['All'];
    headings = new PushHeadings();
    contents = new PushContents();
    url?: string;
    template_id?: string;
    data?: any;
    web_buttons?= new Array<PushContents>();
    chrome_web_icon?: string;
    chrome_web_image?: string;
    chrome_web_badge?: string;
    send_after?: string;
    delayed_option?: string;
    delivery_time_of_day?: string;
    filters?= new Array<PushFilter>();
    web_push_topic: string = now().toString();

    public parseFilter(filter: string): Array<PushFilter> {
        if (filter == "") return null;

        let conditions = filter.split(/(&&|\|\|)/);

        let filters = new Array<PushFilter>();
        console.log(conditions);
        conditions.forEach(condition => {

            let tokens = condition.split(/(=|<=|>=|!=|<|>|exists|not_exists)/)
            let cfilter = new PushFilter();
            for (var i = 0; i < tokens.length; i++) {

                const token = tokens[i];

                let litrals = token.split('.');

                if ((litrals.length < 1 || litrals.length > 2) && i <= 1) {
                    cfilter = null;
                    break;
                }

                if (i == 0) {
                    if (litrals.length == 2) {
                        cfilter.field = <"last_session" | "first_session" | "session_count" | "session_time" | "amount_spent" | "bought_sku" | "tag" | "language" | "app_version" | "location" | "email" | "country">litrals[0];
                        cfilter.key = litrals[1];
                    }
                    else {
                        switch (litrals[0]) {
                            case '&&': {
                                cfilter = null;
                                break;
                            }
                            case '||': {
                                cfilter.operator = 'OR';
                                break;
                            }
                            default: {
                                cfilter.field = <"last_session" | "first_session" | "session_count" | "session_time" | "amount_spent" | "bought_sku" | "tag" | "language" | "app_version" | "location" | "email" | "country">litrals[0];
                            }
                        }
                    }

                }
                if (i == 1)
                    if (litrals[0].match(/(=|<=|>=|!=|<|>|exists|not_exists)/))
                        cfilter.relation = <">" | "<" | "=" | "!=" | "exists" | "not_exists">litrals[0];
                if (i == 2)
                    cfilter.value = token;
            }
            if (cfilter) filters.push(cfilter);
        });
        return filters;
    }

}

export class PushHeadings {
    public en: string;
}

export class PushContents {
    public en: string;
}

export class PushWebButton {
    id: string;
    text: string;
    icon: string;
    url: string;
}

export class PushFilter {
    field?: "last_session" | "first_session" | "session_count" | "session_time" | "amount_spent" | "bought_sku" | "tag" | "language" | "app_version" | "location" | "email" | "country";
    key?: string;
    relation?: ">" | "<" | "=" | "!=" | "exists" | "not_exists";
    value?: string;
    operator?: 'OR';
    hours_ago?: string;
    radius?: any;
    lat?: any;
    long?: any;
}