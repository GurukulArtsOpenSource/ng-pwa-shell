
import {throwError as observableThrowError, of as observableOf,  Observable ,  BehaviorSubject } from 'rxjs';
import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { LocalStorageService } from 'angular-2-local-storage';




import { SessionStorageService } from './session.storage';
import { CommonUtilitiesService } from "../_common/common.utils";
import { environment } from '../../environments/environment'
import { map, catchError, tap } from 'rxjs/operators';

export enum AuthModes { Public, Anonymous, Secure }
export enum CacheStrategies { NoCache, Performance, Freshness, CacheFirst }
export enum StorageTypes { Local, Session }

@Injectable()
export class Service {
    private _Name: string = "";
    private _Base_URI: string = "";
    //private _Headers: Headers[];
    private _Headers : any[]
    public _RequestOptions: {headers?: HttpHeaders}[];
    public GETQueryConfig = {
        "type": "param",
        "key": ""
    }


    ngOnInit(): void {
        this._Base_URI = environment.base.api;
    }

    constructor(private _http: HttpClient, private cache: LocalStorageService, private session: SessionStorageService, private _utils: CommonUtilitiesService) {
        if (this._Headers == undefined) this._Headers = new Array<any>();
        if (this._RequestOptions == undefined) this._RequestOptions = new Array<{headers?: HttpHeaders}>();


        /* Default Services */
        if (this._Name == undefined) this._Name = "";
        if (this._Base_URI == undefined) this._Base_URI = "";

        /* Default Headers */
        /*
        this.SetHeaders(AuthModes.Public,{});
        this.SetHeaders(AuthModes.Anonymous,{});
        this.SetHeaders(AuthModes.Secure,{});
        */
    }

    get(url: string, filter?: any, CacheStrategy: CacheStrategies = CacheStrategies.NoCache, StorageType: StorageTypes = StorageTypes.Local, AuthMode: AuthModes = AuthModes.Secure): Observable<any> {
        let qp = "";
        qp = this.FiltertoQuery(filter);
        let _url = url + (qp == "" ? "" : ((url.indexOf('?') >= 0 ? '&' : '?') + qp));
        let $Storage = StorageType == StorageTypes.Local ? this.cache : this.session;
        //console.log(url + " - " + url.indexOf("http"));
        let BaseURI = url.indexOf("http") >= 0 ? '' : this._Base_URI;

        if (!this._utils.isOnline) CacheStrategy = CacheStrategies.CacheFirst;

        switch (CacheStrategy) {
            case CacheStrategies.NoCache: {
                return this._http.get(BaseURI + _url, this._RequestOptions[AuthMode]).pipe(
                    map(response => <any>response),
                    catchError(this.handleError)
                );
            }
            case CacheStrategies.CacheFirst: {
                if ($Storage.get("GET " + _url) != null) {
                    let response: any;
                    response = $Storage.get("GET " + _url);
                    return observableOf(JSON.parse(response));
                }
                else {
                    return this._http.get(BaseURI + _url, this._RequestOptions[AuthMode]).pipe(   
                        map(response => <any>response),
                        tap(data => {
                            $Storage.set("GET " + _url, JSON.stringify(data));
                        }),
                        catchError(this.handleError)
                    );
                }
            }
            case CacheStrategies.Performance: {
                if ($Storage.get("GET " + _url) != null) {
                    let response: any;
                    response = $Storage.get("GET " + _url);
                    let bsres = new BehaviorSubject(JSON.parse(response));
                    this._http.get(BaseURI + _url, this._RequestOptions[AuthMode])
                        .subscribe(response => {
                            $Storage.set("GET " + _url, JSON.stringify(response));
                            bsres.next(response);
                        },err => this.handleError(err));
                    return bsres.asObservable();
                }
                else {
                    return this._http.get(BaseURI + _url, this._RequestOptions[AuthMode]).pipe(
                        map( response => <any>response),
                        tap(data => {
                            $Storage.set("GET " + _url, JSON.stringify(data));
                        }),
                        catchError(this.handleError)
                    );
                }
            }
            case CacheStrategies.Freshness: {
                return this._http.get(BaseURI + _url, this._RequestOptions[AuthMode]).pipe(                
                    map(response => <any>response),
                    tap(data => {
                        $Storage.remove("GET " + _url);
                        $Storage.set("GET " + _url, JSON.stringify(data));
                    }),
                    catchError(this.handleError)
                );
            }

        }
        return this._http.get(BaseURI + _url, this._RequestOptions[AuthMode]).pipe(
            map(response => <any>response),
            catchError(this.handleError)
        );
    }

    post(url: string, model: any, headers?: any, AuthMode: AuthModes = AuthModes.Secure): Observable<any> {
        let _body: any;
        let isnewHeader = (headers != undefined);
        //console.log(model.constructor === Object);
        if (model.constructor === Object)
            _body = JSON.stringify(model);
        else _body = model;
        let _url = this._Base_URI + url;

        let OriginalHeaders: any;
        let NewHeaders: any;
        let NewRO : any;
        if (isnewHeader) {
            OriginalHeaders = this._Headers[AuthModes.Public];
            NewHeaders = Object.assign({}, headers, this._Headers[AuthModes.Anonymous], this._Headers[AuthModes.Secure]);
            NewRO = { headers: NewHeaders };
        }
        return this._http.post(_url, _body, isnewHeader ? NewRO : this._RequestOptions[AuthMode]).pipe(
            map(response => <any>response),
            catchError(this.handleError)
        );

    }

    put(url: string, model: any, AuthMode: AuthModes = AuthModes.Secure): Observable<any> {
        let _body = JSON.stringify(model);
        let _url = this._Base_URI + url;

        return this._http.put(_url, _body, this._RequestOptions[AuthMode]).pipe(
            map(response => <any>response),
            catchError(this.handleError));
    }

    delete(url: string, model: any, AuthMode: AuthModes = AuthModes.Secure): Observable<any> {
        let _body = JSON.stringify(model);
        let _url = this._Base_URI + url;
        let requestOption = Object.assign({}, this._RequestOptions[AuthMode], { 'body' : _body });
        return this._http.delete(_url, requestOption).pipe(
            map(response => <any>response),
            catchError(this.handleError)
        );
    }

    private handleError(error: Response) {
        //console.error("error handleError");
        //return Observable.throw(error.json().error || 'Server error');
        let response = error.text().toString()
        return observableThrowError(response);
    }

    SetHeaders(AuthMode: AuthModes, AuthHeaders: any): void {
        this._Headers[AuthMode] = AuthHeaders;
        //console.log(AuthMode + '-' + JSON.stringify(AuthHeaders));

        let headers = new HttpHeaders(Object.assign({}, this._Headers[AuthModes.Public]));
        this._RequestOptions[AuthModes.Public] = { headers: headers };

        headers = new HttpHeaders(Object.assign({}, this._Headers[AuthModes.Public], this._Headers[AuthModes.Anonymous]));
        this._RequestOptions[AuthModes.Anonymous] = { headers: headers };

        headers = new HttpHeaders(Object.assign({}, this._Headers[AuthModes.Public], this._Headers[AuthModes.Anonymous], this._Headers[AuthModes.Secure]));
        this._RequestOptions[AuthModes.Secure] = { headers: headers };


    }
    UnsetHeaders(header: AuthModes): void {
        this.SetHeaders(header, {});
    }

    Initialize(Name: string, BaseURI: string): void {
        this._Name = Name;
        this._Base_URI = BaseURI;
    }

    JSONtoParams(obj: any) {
        let params = new URLSearchParams();
        for (let key in obj) {
            params.set(key, obj[key])
        }
        return params;
    }
    FiltertoQuery(obj: any): string {
        let qp: string = "";

        if (this.GETQueryConfig.type == "param") {
            if (obj) qp = this.JSONtoParams(obj).toString();
        }
        else if (this.GETQueryConfig.type == "json") {
            if (obj) qp = this.GETQueryConfig.key + "=" + JSON.stringify(obj);
        }
        else qp = "";

        return qp;
    }
}
