import { Component, Injectable } from "@angular/core";
import { ActivatedRoute, Router } from "@angular/router";
import { Service } from "./api";
import { AuthService } from "./auth";
import { CommonDataService } from "../_common/common.data";
import { LocalStorageService } from "angular-2-local-storage";
import { SessionStorageService } from "./session.storage";
import { environment } from '../../environments/environment';
import { MatSnackBar } from "@angular/material";
import { HotkeysService, Hotkey } from "angular2-hotkeys";
import { CommonUtilitiesService } from "../_common/common.utils";
import { PushService } from "./push";
import { CommonUIService } from "../_common/common.ui";


@Injectable()
export class Utilities {
    public Router = this._Router;
    public Api = this._Api;
    public Auth = this._Auth;
    public Cache = this._Cache;
    public Session = this._Session;
    public Environment = environment;
    public SnackBar = this._SnackBar;
    public Hotkey = this._hotkeysService;
    public Data = this._Data;
    public UI = this._UI;
    public Utils = this._Utils;
    public Push = this._Push;
    

    constructor(
        private _Api: Service,
        private _Auth: AuthService,
        private _Cache : LocalStorageService,
        private _Session : SessionStorageService,
        private _Router: Router,
        private _SnackBar: MatSnackBar,
        private _hotkeysService: HotkeysService,
        private _Data : CommonDataService,
        private _UI : CommonUIService,
        private _Utils : CommonUtilitiesService,
        private _Push : PushService
    ) {}

    public has(AccessPermission : string) : boolean {
        return this._Auth.hasAccess(AccessPermission);
    }

}


@Component({
    selector: 'base-component',
    template: ''
})
export class BaseComponent {
    private $Router = this.$.Router;
    private $Api = this.$.Api;
    private $Auth = this.$.Auth;
    private $Cache = this.$.Cache;
    private $Session = this.$.Session;
    private $Environment = this.$.Environment;
    private $SnackBar = this.$.SnackBar;
    private $Hotkey = this.$.Hotkey;
    private $Data = this.$.Data;
    private $Utils = this.$.Utils;
    private $Push = this.$.Push;
    
    constructor(public $ : Utilities) { }

}