import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { Utilities } from "./_core/utilities";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {

  title = 'app';

  constructor (private $ : Utilities) {

  }

  ngOnInit(): void {

    this.$.Api.ngOnInit(); //Calling Custom $.Api.ngOnInit() to initialize common utilities.
    this.$.Api.GETQueryConfig.type = "json";
    this.$.Api.GETQueryConfig.key = "query";
    this.$.Auth.ngOnInit(); //Calling Custom $.Auth.ngOnInit() to initialize common utilities.
    this.$.Push.ngOnInit(); //Calling Custom $.Push.ngOnInit() to initialize common utilities.
    this.$.Utils.ngOnInit(); //Calling Custom $.Utils.ngOnInit() to initialize common utilities.
    this.$.Data.ngOnInit(); //Calling Custom $.Data.ngOnInit() to initialize common utilities.
    this.$.UI.ngOnInit();

    this.$.Auth.Access.push("USER"); //Access Permission Key

  }

  ngOnDestroy(): void {
		
	  }


}
