import { TestBed, inject } from '@angular/core/testing';

import { SubComponent } from './sub.component';

describe('a sub component', () => {
	let component: SubComponent;

	// register all needed dependencies
	beforeEach(() => {
		TestBed.configureTestingModule({
			providers: [
				SubComponent
			]
		});
	});

	// instantiation through framework injection
	beforeEach(inject([SubComponent], (SubComponent) => {
		component = SubComponent;
	}));

	it('should have an instance', () => {
		expect(component).toBeDefined();
	});
});