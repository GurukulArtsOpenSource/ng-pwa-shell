import { Injectable, TemplateRef } from "@angular/core";
import { BehaviorSubject } from "rxjs";

@Injectable()
export class CommonUIService {
  public Loader: boolean = false;
  public TopbarExtras: TemplateRef<any>;
  public SidebarExtras: TemplateRef<any>;
  public NavigationItems = Array<NavigationItem>();
  public PageTitle: string;

  constructor() { }

  public ngOnInit(): void {

    //  Called at AppComponent.ngOnInit() :: (Not a Native ngOnInit function)
    //  As ngOnInit doesn't execute in Services/Injectable Class
    this.NavigationItems.push(new NavigationItem("Home","/home",null,"home","Home"));
    this.NavigationItems.push(new NavigationItem("Sub Page","/sub",null,"event","",null,"ADMIN"));


  }


}

export class NavigationItem {
  icon: string = "border_outer";
  color: string = "primary";
  caption: string = "My Tab";
  tooltip: string = "My Tab";
  link: string = "/";
  isDivider: boolean = false;
  has: string | string[] = "";

  constructor(caption? : string, link? : string, color? : string, icon? : string, tooltip?: string, isDivider?:boolean, has?:string | string[] ) {
    if(caption) this.caption = caption;
    if(icon) this.icon = icon;
    if(color) this.color = color;
    if(tooltip) this.tooltip = tooltip;
    if(link) this.link = link;
    this.isDivider = isDivider;    
    if(has) this.has = has;
  }

}