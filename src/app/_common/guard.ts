import { Injectable } from '@angular/core';
import { Router, CanActivate, CanActivateChild, ActivatedRouteSnapshot, RouterStateSnapshot, ActivatedRoute } from '@angular/router';
import { AuthService } from '../_core/auth';
import { Observable , of as observableOf } from 'rxjs';
import { LocalStorageService } from 'angular-2-local-storage';
import { Service, AuthModes } from '../_core/api';
import { catchError, map} from 'rxjs/operators';
import { MatSnackBar } from '@angular/material';

@Injectable()
export class AuthGuard implements CanActivate, CanActivateChild {
    constructor(private _Api: Service, private _router: Router, private _Auth: AuthService, private _Cache: LocalStorageService, private sb : MatSnackBar) { }
    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean | Observable<boolean> {

        this._Auth.redirectUrl = state.url;
        let has = [];
        if(route.data["has"])
        has = route.data["has"] as Array<string>;
        
        return this._Auth.isAlive().pipe(
            map(e => {
                if(this._Auth.hasAccess(has))
                return true;
                else
                {
                    this.sb.open('Access Denied!',"Close",{duration : 2000});
                    this._router.navigate(['/']);
                }
            })
            ,
            catchError((error) => {
                //console.log("error Guard Error");
                //console.log("Token Dead or Not Found");
                this._router.navigate(['/signin']);
                return observableOf(false);
            })
        );
    }

    canActivateChild() {

        return true;
    }

}