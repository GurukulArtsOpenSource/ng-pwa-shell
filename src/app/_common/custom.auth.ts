
import { throwError as observableThrowError, of as observableOf, Observable, Subscriber, throwError } from 'rxjs';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';


import { environment } from '../../environments/environment';
import { AuthModes, Service } from '../_core/api';
import { LocalStorageService } from 'angular-2-local-storage';
import { Md5 } from 'ts-md5/dist/md5';
import { LoggedInUser, ICustomLoginService } from '../_core/auth';
import { HttpClient } from '@angular/common/http';
import { map, catchError } from 'rxjs/operators';

/*
    This Service (Injectable Class) is for creating Custom Login Service.
    This service is used by ../core.auth.ts for Custom Provider

    the class must implement ICustomLoginService to be able to be used in ../core.auth.ts


*/

@Injectable()
export class CustomAuthService implements ICustomLoginService {

    public _base_uri = "http://localhost:3004";

    constructor(private _Api: Service, private _http: HttpClient, private _Router: Router, private _Cache: LocalStorageService) { }

    public ngOnInit(): void {

        //  Called at Auth.ngOnInit() :: (Not a Native ngOnInit function)
        //  As ngOnInit doesn't execute in Services/Injectable Class



    }

    login(UserCred: any): Observable<LoggedInUser> {
        let User: LoggedInUser;
        //let headers = new Headers({ 'Content-Type': 'application/json' });
        //let options = new RequestOptions({ headers: headers }); use for post
        let filter = this._Api.JSONtoParams({ "username": UserCred.u, "password": UserCred.p });
        return this._http.get(this._base_uri + "/users?" + filter)
        .pipe(
            map(token => {
                let res = <Array<LoggedInUser>>token;
                if (res) {
                    let ures = res[0]
                    console.log(res);
                    let header = { "Authorization": ures.token };
                    this._Api.SetHeaders(AuthModes.Secure, header);
                    this._Cache.set('Auth.Token', header);
                    let user = {
                        id: ures.id,
                        name: ures.name,
                        token: ures.token,
                        provider: 'custom'
                    };
                    User = user;
                    this._Cache.set('Auth.User', User);
                    return User;
                }
            }),
            catchError(error => {
                return observableOf(error);
            })
        );
    }

    isAlive(UserState: any): Observable<LoggedInUser> {
        //console.log("isAlive");
        let User: LoggedInUser;

        let token: any = this._Cache.get('Auth.Token');
        if (token) {
            this._Api.SetHeaders(AuthModes.Secure, token);
            return this._http.get(this._base_uri + "/users?token=" + token.Authorization)
                .pipe(
                    map(res => {
                        let ures = <Array<LoggedInUser>>res;
                        if (ures.length) {
                            User = this._Cache.get('Auth.User');
                            return User;
                        }
                        else {
                            this._Cache.remove('Auth.Token');
                            this._Cache.remove('Auth.User');
                            observableThrowError(false);
                        }
                    }),
                    catchError((error) => {
                        this._Cache.remove('Auth.Token');
                        this._Cache.remove('Auth.User');
                        return observableThrowError(error);
                    })
                );
        }
        else {
            return observableThrowError(false);
        }

    }

    logout(UserState: any): Observable<boolean> {
        this._Cache.remove('Auth.Token');
        this._Cache.remove('Auth.User');

        return this._http.get(this._base_uri + "/users").pipe(
        map(
            res => {
                if(res)
                return true;
            }
        ),
        catchError((error) => {
            //console.log(error);
            //console.log("isAlive Error");
            return observableOf(false);
        })
    );
    }
}