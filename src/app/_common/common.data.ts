import { Injectable } from "@angular/core";
import { CommonUIService } from "./common.ui";
import { Observable, Subscriber } from "rxjs";
import { share } from "rxjs/operators";

@Injectable()
export class CommonDataService {
  Loader: boolean = false;
  Data: any = [];
  public AppInit$: Observable<boolean>;
  public AppInitSubscriber: Subscriber<boolean> = new Subscriber<boolean>();

  constructor(private _ui: CommonUIService) { }

  public ngOnInit() : void { 
    
    //  Called at AppComponent.ngOnInit() :: (Not a Native ngOnInit function)
    //  As ngOnInit doesn't execute in Services/Injectable Class
    this.AppInit$ = new Observable<boolean>((observer: Subscriber<boolean>) => this.AppInitSubscriber = observer).pipe(share());

  }

  public SetData(MyData: any[]) {
    this.Data = MyData;
  }

}
