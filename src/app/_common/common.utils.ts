
import {fromEvent as observableFromEvent, of as observableOf, merge as observableMerge,  Observable } from 'rxjs';

import {map} from 'rxjs/operators';
import { Injectable } from "@angular/core";



import { PageEvent } from "@angular/material";

function getWindow (): any {
  return window;
}




@Injectable()
export class CommonUtilitiesService {
  public Keys = Object.keys; //To Search in the JSON keys :: Native JS Object.keys
  public isOnline = navigator.onLine; //To check internet status :: Native JS navigator.onLine
  public Connection$: Observable<boolean>; //Trigger to check internet status :: Native JS navigator.onLine

  constructor() {

    //Creating a Trigger once the window state change
    this.Connection$ = observableMerge(
      observableOf(navigator.onLine),
      observableFromEvent(window, 'online').pipe(map(() => true)),
      observableFromEvent(window, 'offline').pipe(map(() => false)));

  }

  public ngOnInit(): void {

    //  Called at AppComponent.ngOnInit() :: (Not a Native ngOnInit function)
    //  As ngOnInit doesn't execute in Services/Injectable Class

    //Connection trigger subscriber
    this.Connection$.subscribe(state => {
      this.isOnline = state;
    });

  }

  /** Start writing your common functions here */

  public MyFoo(MyData: any[]) {
    return true;
  }

  public window() : any {
    return getWindow();
  }

}

export interface Deserializable<T> {
  deserialize(input: any): T;
  // https://nehalist.io/working-with-models-in-angular/
}

export class Dictionary<T> {

  private count: number = 0;
  private items: T[] = [];

  public has(key: string): boolean {
    return this.items.hasOwnProperty(this._p(key));
  }

  public length() {
    return this.count;
  }

  public set(key: string, value: T) {
    if (!this.items.hasOwnProperty(this._p(key)))
      this.count++;
    //console.log(this._p(key));
    this.items[this._p(key)] = value;
  }


  public get(key: string): T {
    return this.items[this._p(key)];
  }

  public remove(key: string): T {
    var val = this.items[this._p(key)];
    delete this.items[this._p(key)];
    this.count--;
    return val;
  }

  public keys(): string[] {
    var keySet: string[] = [];

    for (var prop in this.items) {
      if (this.items.hasOwnProperty(prop)) {
        keySet.push(this._k(prop));
      }
    }

    return keySet;
  }

  public values(): T[] {
    var _values: T[] = [];

    for (var prop in this.items) {
      if (this.items.hasOwnProperty(prop)) {
        _values.push(this.items[prop]);
      }
    }

    return _values;
  }


  public clear() {
    this.items = [];
    this.count = 0;
  }

  public filter(fn: (element, index, array) => boolean): T[] {
    let result: T[] = [];
    result = this.values().filter(fn);
    //result = this.items;
    return result;
  }

  public push(keyColumn: string, items: any[]): void {
    if (!Array.isArray(items)) return;
    items.forEach(element => {
      //console.log(element[keyColumn]);
      this.set(element[keyColumn], element);
    });
  }

  private _p(key: string): string { return isNaN(+key) ? key : '+' + key; }
  private _k(key: string): string { return isNaN(+key) ? key : key.substr(1); }


}


export class ViewModel {
  private _VM: any[] = [];
  values: any[] = [];
  pageIndex = 0;
  length = 1000;
  size = 25;
  options = [10, 25, 50, 100];

  //
  updateValues(): void {
    let returnArray: any = [];
    if (this._VM.length) {
      for (var index = this.pageIndex * this.size; index < (this.pageIndex + 1) * this.size; index++) {
        var element = this._VM[index];
        if (element) returnArray.push(element);
      }
    }
    //console.log(returnArray);
    this.values = returnArray;
  }

  get VM(): any[] {
    return this._VM;
  }
  set VM(data: any[]) {
    this.pageIndex = 0;
    this.length = data.length;
    this._VM = data;
    this.updateValues();
  }
  change($e: PageEvent) {
    //console.log($e);
    this.pageIndex = $e.pageIndex;
    this.size = $e.pageSize;
    this.updateValues();
  }

  clear() {

  }

}