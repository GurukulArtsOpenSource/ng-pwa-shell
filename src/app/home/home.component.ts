import { Component, OnInit, TemplateRef, ViewChild } from '@angular/core';
import { LoggedInUser } from "../_core/auth";
import { BaseComponent, Utilities } from "../_core/utilities";
import {MediaMatcher} from '@angular/cdk/layout';

@Component({
	selector: 'home',
	templateUrl: 'home.component.html'
})

export class HomeComponent implements OnInit {

	
	@ViewChild('TopbarExtras')
	private TopbarExtras: TemplateRef<any>

	@ViewChild('SidebarExtras')
	private SidebarExtras: TemplateRef<any>
	

	public user: LoggedInUser;
	private loggedIn: boolean;

	constructor(private $ : Utilities) { }
	

	

	ngOnInit() {
		this.$.Auth.isAlive().subscribe((user) => {
			this.user = user;
			this.loggedIn = (user != null);
		});
		
		this.$.UI.PageTitle = "Home";

		this.$.UI.TopbarExtras = this.TopbarExtras;
		this.$.UI.SidebarExtras = this.SidebarExtras;
	 }

	 check(){
		this.$.Auth.isAlive().subscribe((user) => {
			console.log(user);
			this.user = user;
			this.loggedIn = (user != null);
		}); 
	 }

	 signOut() {
		 this.$.Auth.logout().subscribe(res => {
			 this.user =null;
			 this.loggedIn = false;
  			  this.$.Router.navigate(['/signin']);
  
		 });
	 }
}