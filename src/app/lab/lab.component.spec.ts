import { TestBed, inject } from '@angular/core/testing';

import { LabComponent } from './lab.component';

describe('a lab component', () => {
	let component: LabComponent;

	// register all needed dependencies
	beforeEach(() => {
		TestBed.configureTestingModule({
			providers: [
				LabComponent
			]
		});
	});

	// instantiation through framework injection
	beforeEach(inject([LabComponent], (LabComponent) => {
		component = LabComponent;
	}));

	it('should have an instance', () => {
		expect(component).toBeDefined();
	});
});