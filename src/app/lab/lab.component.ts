import { Component, OnInit } from '@angular/core';

@Component({
	selector: 'lab',
	templateUrl: 'lab.component.html'
})

export class LabComponent implements OnInit {

	ngOnInit() { }


	public previewFile(event) {
		const reader = new FileReader();
		console.log("atpreview");
		reader.onload = (e: any) => {
			console.log('csv content', e.target.result);
		};
		//reader.readAsDataURL(event.target.files[0]);
		reader.readAsText(event.target.files[0]);
	}

}