import { Component, OnInit, Output, EventEmitter, TemplateRef, ViewChild } from '@angular/core';
import { Utilities } from '../_core/utilities';

@Component({
	selector: 'top-bar',
	templateUrl: 'top-bar.component.html'
})

export class TopBarComponent implements OnInit {
	@Output() 
	onToggleMenu: EventEmitter<any> = new EventEmitter();
	
	@ViewChild('defaultExtras')
	private defaultExtras : TemplateRef<any>
	
	constructor(public $ : Utilities){

	}

	ngOnInit() { 
		this.$.UI.TopbarExtras = this.defaultExtras;
	}


	toggle() {

		this.onToggleMenu.emit(null);
	}
}