import { TestBed, inject } from '@angular/core/testing';

import { TopBarComponent } from './top-bar.component';

describe('a top-bar component', () => {
	let component: TopBarComponent;

	// register all needed dependencies
	beforeEach(() => {
		TestBed.configureTestingModule({
			providers: [
				TopBarComponent
			]
		});
	});

	// instantiation through framework injection
	beforeEach(inject([TopBarComponent], (TopBarComponent) => {
		component = TopBarComponent;
	}));

	it('should have an instance', () => {
		expect(component).toBeDefined();
	});
});