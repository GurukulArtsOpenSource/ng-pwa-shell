// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.

export const environment = {
  production: false,
  SocialLoginIds: {
    google: "77917362825-569k6obtuu5u4iidpg20pu9hveubk4np.apps.googleusercontent.com",
    // facebook : "facebook_provider_id",
    // linkedin : "linkedin_provider_id"
  },
  OneSignal: {
    api : "https://onesignal.com/api/v1/",
    appId: "dc34d475-4767-4b9a-9a82-9efc9160f9b9",
    restKey: "NmY5OGY1NWYtMjMwMy00M2MzLTk1ODYtOTJlNTBiZmI3ZGYz",
  },
  base: {
    href: window.location.origin,
    api: "https://localhost:3004"
  },
  app: {
    version: require('../../package.json').version || '0.0.0',
    author : require('../../package.json').author || { name: "Gurukul Arts", email : "arts@gurukul.org"},
    repository : require('../../package.json').repository || { type: "git", url : "https://gitlab.com/GurukulArtsSeva/ng-pwa-shell"},
    prefix: require('../manifest.json').prefix || 'pwa',
    short_name: require('../manifest.json').short_name || 'Angular PWA',
    name: require('../manifest.json').name || 'Angular 6 PWA',
    description: require('../manifest.json').description || 'My App for Angular 6 PWA',
    start_url: require('../manifest.json').start_url || '/',
    scope: require('../manifest.json').scope || '/',
    display: require('../manifest.json').display || 'standalone',
    orientation: require('../manifest.json').orientation || 'portrait',
    icons: require('../manifest.json').icons,
    theme_color: require('../manifest.json').theme_color || '#a7437c',
    background_color: require('../manifest.json').background_color || '#fff',
  }
};
