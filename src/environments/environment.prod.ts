export const environment = {
  production: true,
  SocialLoginIds: {
    google: "77917362825-569k6obtuu5u4iidpg20pu9hveubk4np.apps.googleusercontent.com",
    // facebook : "facebook_provider_id",
    // linkedin : "linkedin_provider_id"
  },
  OneSignal: {
    api : "https://onesignal.com/api/v1/",
    appId: "bbb12f8e-e574-4f51-805f-bd8fc282e745",
    restKey: "MzYyNGI1OTYtZmRkMS00NTdlLTllYjQtNGEzNmZiZDI2ZmUz",
  },
  base: {
    href: window.location.origin,
    api: "https://baas.kinvey.com/appdata/kid_Sy5Qsx-af"
  },
  app: {
    version: require('../../package.json').version || '0.0.0',
    author : require('../../package.json').author || { name: "Gurukul Arts", email : "arts@gurukul.org"},
    repository : require('../../package.json').repository || { type: "git", url : "https://gitlab.com/GurukulArtsSeva/ng-pwa-shell"},
    prefix: require('../manifest.json').prefix || 'pwa',
    short_name: require('../manifest.json').short_name || 'Angular PWA',
    name: require('../manifest.json').name || 'Angular 6 PWA',
    description: require('../manifest.json').description || 'My App for Angular 6 PWA',
    start_url: require('../manifest.json').start_url || '/',
    scope: require('../manifest.json').scope || '/',
    display: require('../manifest.json').display || 'standalone',
    orientation: require('../manifest.json').orientation || 'portrait',
    icons: require('../manifest.json').icons || [],
    theme_color: require('../manifest.json').theme_color || '#a7437c',
    background_color: require('../manifest.json').background_color || '#fff',
  }

};
